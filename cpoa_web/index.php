<?php
	include_once 'model/connectBdd.php';
	$messageType="null";
	session_start();
	if(isset($_POST["logout"])){
		unset($_SESSION["connected"]);
	}
	$fail=false;
	if(isset($_POST["login"])&&isset($_POST["password"])){
		include_once 'model/UserManager.class.php';
		$userManage=new userManager();
		$user=$userManage->verifyUser($_POST["login"],$_POST["password"]);
		if($user==false){
			$fail=true;
			$messageType="error";
			$messageText="Fail to login, wrong user or password";
		}else{
			$userManage->connectUser($user);
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Acceuil - Festival de Cannes</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="vue/style/main.css">
</head>
<body>
	<?php
	if ($messageType!="null") {
	?>
	<div class="bandeau <?php echo $messageType ?>"> <?php echo $messageText?> </div>
	<?php
	}
	?>
	<h1>Acceuil</h1>
	<?php
		if(!isset($_SESSION["connected"])){
			include_once 'vue/template/connection.php';
		}else{
			include_once 'vue/template/actions.html';
		}
	?>
</body>
</html>