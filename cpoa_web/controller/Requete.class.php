<?php
include_once __DIR__.'/../model/BddConnectionManager.class.php';
?>
<?php
include_once __DIR__.'/../model/tool.php';
?>
<?php
/**
 * class acteur
 */
class Requete
{
	private $title="";
	private $description="";
	private $queryDate="";
	private $forThe="";
	private $id=null;
	
	private static function verifyTitle($name){
		if($name=="")return false;
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>128)return false;
		return true;
	}
	private static function verifyDescription($text){
		if(strpos($text, "<"))return false;
		if(strpos($text, ">"))return false;
		if(strpos($text, "&"))return false;
		if(strpos($text, ";"))return false;
		return true;
	}
	private static function verifyQueryDate($date){
		if(!isValideDate($date,'Y-m-d'))return false;
		if($date>date('Y-m-d'))return false;
		return true;
	}
	private static function verifyForThe($date){
		if(!isValideDate($date,'Y-m-d'))return false;
		if($date<date('Y-m-d'))return false;
		return true;
	}
	private static function verifyPersonneId($num){
		if(!is_numeric($num))return false;
		$num=(int)$num;
		if(!is_int($num))return false;
		return true;
	}
	private static function verifyId($num){
		if(is_null($num)||$num=="null"){
			return true;
		}
		if(!is_numeric($num))return false;
		$num=(int)$num;
		if(!is_int($num))return false;
		return true;
	}

	function __construct()
	{
	}

	public function setTitle($name)
	{
		if(!$this->verifyTitle($name))return false;
		$this->title=$name;
		return true;
	}
	public function setDescription($text)
	{
		if($this->description!=$text&&!$this->verifyDescription($text))return false;
		$this->description=$text;
		return true;
	}
	public function setQueryDate($date)
	{
		if(!$this->verifyQueryDate($date))return false;
		$this->queryDate=$date;
		return true;
	}
	public function setForThe($date)
	{
		if(!$this->verifyForThe($date))return false;
		$this->forThe=$date;
		return true;
	}
	public function setPersonneId($num)
	{
		if(!$this->verifyPersonneId($num))return false;
		$this->id=$num;
		return true;
	}
	public function setId($num)
	{
		if(!$this->verifyId($num))return false;
		$this->id=$num;
		return true;
	}
	public function getTitle()
	{
		return $this->title;
	}
	public function getDescription()
	{
		return $this->description;
	}
	public function getQueryDate()
	{
		return $this->queryDate;
	}
	public function getForThe()
	{
		return $this->forThe;
	}
	public function getPersonneId()
	{
		return $this->id;
	}
	public function getId()
	{
		return $this->id;
	}
	public function toArray()
	{
		return array($this->id,$this->title,$this->description,$this->queryDate,$this->forThe);
	}
}
?>