<?php
include_once __DIR__.'/../model/BddConnectionManager.class.php';
?>
<?php
include_once __DIR__.'/../model/tool.php';
?>
<?php
/**
 * class acteur
 */
class VIP
{
	private $id=null;
	private $firstName="";
	private $lastName="";
	private $nationality="";
	private $sexe="";
	private $photo="";
	private $tel="";
	private $mail="";
	private $description="";
	
	private static function verifyId($num){
		if(is_null($num)||$num=="null"){
			return true;
		}
		if(!is_numeric($num))return false;
		$num=(int)$num;
		if(!is_int($num))return false;
		return true;
	}
	private static function verifyFirstName($name){
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		return true;
	}
	private static function verifyLastName($name){
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		return true;
	}
	private static function verifyNationality($name){
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		if(!isDefine("nationality","label",$name))return false;
		return true;
	}
	private static function verifySexe($name){
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		if(!isDefine("sexe","label",$name))return false;
		return true;
	}
	private static function verifyPhoto($path){
		if(is_null($path)||$path=="null"){
			return true;
		}
		if(strpos($path, "<"))return false;
		if(strpos($path, ">"))return false;
		if(strpos($path, "&"))return false;
		if(strpos($path, ";"))return false;
		if(strlen($path)>20)return false;
		return true;
	}
	private static function verifyMail($email){
		if(is_null($email)||$email=="null"){
			return true;
		}
		if(strpos($email, "<"))return false;
		if(strpos($email, ">"))return false;
		if(strpos($email, "&"))return false;
		if(strpos($email, ";"))return false;
		if(strlen($email)>256)return false;
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}
		return false;
	}
	private static function verifyDescription($name){
		if(is_null($name)||$name=="null"){
			return true;
		}
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		return true;
	}
	private static function verifyTel($tel){
		if(is_null($tel)||$tel=="null"){
			return true;
		}
		if(strlen($tel)>32)return false;
		$tel=str_replace(" ", "", $tel);
		if(!startWith($tel,"+"))return false;
		return is_numeric($tel);
	}

	function __construct()
	{
	}

	public function setFirstName($name)
	{
		if($this->firstName!=$name&&!$this->verifyFirstName($name))return false;
		$this->firstName=$name;
		return true;
	}
	public function setLastName($name)
	{
		if($this->lastName!=$name&&!$this->verifyLastName($name))return false;
		$this->lastName=$name;
		return true;
	}
	public function setNationality($name)
	{
		if($this->nationality!=$name&&!$this->verifyNationality($name))return false;
		$this->nationality=$name;
		return true;
	}
	public function setSexe($name)
	{
		if($this->sexe!=$name&&!$this->verifySexe($name))return false;
		$this->sexe=$name;
		return true;
	}
	public function setPhoto($path)
	{
		if($this->photo!=$path&&!$this->verifyPhoto($path))return false;
		$this->photo=$path;
		return true;
	}
	public function setDescription($text)
	{
		if($this->description!=$text&&!$this->verifyDescription($text))return false;
		$this->description=$text;
		return true;
	}
	public function setTel($tel)
	{
		if($this->tel!=$tel&&!$this->verifyTel($tel))return false;
		$this->tel=$tel;
		return true;
	}
	public function setMail($mail)
	{
		if($this->mail!=$mail&&!$this->verifyMail($mail))return false;
		$this->mail=$mail;
		return true;
	}
	public function setId($num)
	{
		if($this->id!=$num&&!$this->verifyId($num))return false;
		$this->id=$num;
		return true;
	}
	public function getFirstName()
	{
		return $this->firstName;
	}
	public function getLastName()
	{
		return $this->lastName;
	}
	public function getNationality()
	{
		return $this->nationality;
	}
	public function getDescription()
	{
		return $this->description;
	}
	public function getSexe()
	{
		return $this->sexe;
	}
	public function getPhoto()
	{
		return $this->photo;
	}
	public function getTel()
	{
		return $this->tel;
	}
	public function getMail()
	{
		return $this->mail;
	}
	public function getId()
	{
		return $this->id;
	}
}
?>