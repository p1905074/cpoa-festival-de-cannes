<?php
include_once __DIR__.'/../model/BddConnectionManager.class.php';
?>
<?php
include_once __DIR__.'/../model/tool.php';
?>
<?php
/**
 * class User
 */
class User
{
	private $id;
	private $firstName;
	private $lastName;
	private $mail;
	private $tel;
	private $hash;
	private $admin;
	
	private static function verifyId($num){
		if(is_null($num)||$num=="null"){
			return true;
		}
		if(!is_numeric($num))return false;
		$num=(int)$num;
		if(!is_int($num))return false;
		return true;
	}
	private static function verifyFirstName($name){
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		return true;
	}
	private static function verifyLastName($name){
		if(strpos($name, "<"))return false;
		if(strpos($name, ">"))return false;
		if(strpos($name, "&"))return false;
		if(strpos($name, ";"))return false;
		if(strlen($name)>50)return false;
		return true;
	}
	private static function verifyMail($email){
		if(strpos($email, "<"))return false;
		if(strpos($email, ">"))return false;
		if(strpos($email, "&"))return false;
		if(strpos($email, ";"))return false;
		if(strlen($email)>256)return false;
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}
		return false;
	}
	private static function verifyTel($tel){
		if(strlen($tel)>32)return false;
		$tel=str_replace(" ", "", $tel);
		if(!startWith($tel,"+"))return false;
		return is_numeric($tel);
	}
	private static function verifyAdmin($bool){
		if(!is_numeric($bool))return false;
		if($bool!=0&&$bool!=1)return false;
		return true;
	}
	private static function verifyPassword($str){
		if(!is_string($str))return false;
		if(strlen($str)<8)return false;
		$digit=false;
		$lowerCase=false;
		$upperCase=false;
		$special=false;
		for ($i=0; $i < strlen($str) ; $i++) { 
			if(ctype_lower($str[$i])){
				$lowerCase=true;
			}elseif (ctype_upper($str[$i])) {
				$upperCase=true;
			}elseif (ctype_digit($str[$i])) {
				$digit=true;
			}else{
				$special=true;
			}
		}
		if(!$digit)return false;
		if(!$lowerCase)return false;
		if(!$upperCase)return false;
		if(!$special)return false;
		return true;
	}
	function __construct()
	{
	}

	public function setAdmin($bool)
	{
		if($this->admin!=$bool&&!$this->verifyAdmin($bool))return false;
		$this->admin=$bool;
		return true;
	}

	public function setTel($tel)
	{
		if($this->tel!=$tel&&!$this->verifyTel($tel))return false;
		$this->tel=$tel;
		return true;
	}
	public function setMail($mail)
	{
		if($this->mail!=$mail&&!$this->verifyMail($mail))return false;
		$this->mail=$mail;
		return true;
	}
	public function setFirstName($name)
	{
		if($this->firstName!=$name&&!$this->verifyFirstName($name))return false;
		$this->firstName=$name;
		return true;
	}
	public function setLastName($name)
	{
		if($this->lastName!=$name&&!$this->verifyLastName($name))return false;
		$this->lastName=$name;
		return true;
	}
	public function setPassword($str)
	{
		if(!$this->verifyPassword($str))return false;
		$this->hash=md5($str);
		return true;
	}
	public function setHash($str)
	{
		$this->hash=$str;
		return true;
	}
	public function setId($num)
	{
		if($this->id!=$num&&!$this->verifyId($num))return false;
		$this->id=$num;
		return true;
	}

	public function getId()
	{
		return $this->id;
	}
	public function getFirstName()
	{
		return $this->firstName;
	}
	public function getLastName()
	{
		return $this->lastName;
	}
	public function getMail()
	{
		return $this->mail;
	}
	public function getTel()
	{
		return $this->tel;
	}
	public function getHash()
	{
		return $this->hash;
	}
	public function getAdmin()
	{
		return $this->admin;
	}
	public function toArray()
	{
		return array($this->id,$this->firstName,$this->lastName,$this->mail,$this->tel,$this->admin);
	}
}
?>
