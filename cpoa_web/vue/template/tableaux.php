<?php
include_once '../model/connect.php';
if(!isset($table)){
	header("Location:../index.php");
	include_once '../index.php';
	return;
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Table <?php echo $table?></title>
	<link rel="stylesheet" type="text/css" href="style/main.css">
</head>
<body>
	<?php
	if ($messageType!="null") {
	?>
	<div class="bandeau <?php echo $messageType ?>"> <?php echo $messageText?> </div>
	<?php
	}
	?>
	<a href=".." class="home"></a>
	<h1>Table <?php echo $table?></h1>
	<table>
		<tr>
			<td width="500px" colspan="2">
				<form action="edit<?php echo $table?>.php" method="get">
					<button onclick="this.parentElement.submit()" class="add">add row</button>
				</form>
				<button onclick="window.location.href=window.location.pathname" class="reload">reload</button>
			</td>
		</tr>
		<?php
		for ($i=0; $i < sizeof($list); $i++) {
			$row=$list[$i];
		?>
		<tr>
			<td>
				<a href="edit<?php echo $table?>.php?id=<?php echo $row[1]?>"><?php echo $row[0];?></a>
			</td>
			<td width="100px">
				<form method="post">
					<button onclick="this.parentElement.submit()" class="delete">supprimer</button>
					<input type="hidden" name="id" value="<?php echo $row[1]?>">
					<input type="hidden" name="action" value="delete">
				</form>
			</td>
		</tr>
		<?php
		}
		?>
	</table>
</body>
</html>