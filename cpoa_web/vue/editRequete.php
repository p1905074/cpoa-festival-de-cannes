<?php
include_once '../model/RequeteManager.class.php';
$messageType="null";
$requeteManage=new RequeteManager();
if(isset($_GET["id"])){
	$id=$_GET["id"];
}
if(isset($_POST["title"])&&isset($_POST["queryDate"])&&isset($_POST["forThe"])&&isset($_POST["personneId"])&&isset($_POST["description"])){
	if(isset($id)){
		$rep=$requeteManage->editRequete($id,$_POST["title"],$_POST["description"],$_POST["queryDate"],$_POST["forThe"],$_POST["personneId"]);
		if(gettype($rep)===gettype("")){
			$messageType="error";
			$messageText=$rep;
		}else{
			$messageType="info";
			$messageText="1 ligne changé";
		}
	}else{
		$rep=$requeteManage->addRequete("null",$_POST["title"],$_POST["description"],$_POST["queryDate"],$_POST["forThe"],$_POST["personneId"]);
		if(gettype($rep)===gettype("")){
			$messageType="error";
			$messageText=$rep;
		}else{
			$messageType="info";
			$messageText="1 ligne ajouté";
		}
	}

}
if(isset($id)){
	$obj=$requeteManage->getRequete($_GET["id"]);
	$title=$obj->getTitle();
	$queryDate=$obj->getQueryDate();
	$forThe=$obj->getForThe();
	$personneId=$obj->getPersonneId();
	$description=$obj->getDescription();
}else{
	$title="";
	$queryDate="";
	$forThe="";
	$personneId="";
	$description="";
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php
if(isset($id)){
	echo "Edition de ".$title;
}else{
	echo "Ajout d'une requete";
}
?></title>
	<link rel="stylesheet" type="text/css" href="style/main.css">
</head>
<body>
	<?php
	if ($messageType!="null") {
	?>
	<div class="bandeau <?php echo $messageType ?>"> <?php echo $messageText?> </div>
	<?php
	}
	?>
	<a href=".." class="home"></a>
<h1><?php
if(isset($id)){
	echo "Edition de ".$title;
}else{
	echo "Ajout d'une requete";
}
?></h1>
	<a href="listRequete.php"><- go back</a>
<form method="post">
	<label>titre :<input type="text" name="title" value="<?php echo $title?>"></label><br>
	<label>demandé le : <input type="date" name="queryDate" value="<?php echo $queryDate?>"></label>
	<label>pour le : <input type="date" name="forThe" value="<?php echo $forThe?>"></label><br>
	<label>associé a <select style="width:30%" name="personneId">
    <?php
    $identifiers=listColumn("personne","id");
    $firstNames=listColumn("personne","firstName");
    $lastNames=listColumn("personne","lastName");
    for ($i=0; $i < sizeof($identifiers); $i++) { 
    ?>
    <option value="<?php echo $identifiers[$i]?>" <?php if($personneId==$identifiers[$i]){echo "selected=''";} ?>><?php echo $firstNames[$i]." ".$lastNames[$i]; ?></option>
    <?php
    }
    ?>
	</select></label><br>
	description:<br>
	<textarea name=description><?php echo $description?></textarea>
	<input type="submit" name="" value="enregistrer"><br>
</form>
</body>