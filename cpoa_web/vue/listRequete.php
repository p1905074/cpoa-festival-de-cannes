<?php

$table="Requete";
$messageType="null";
include_once '../model/RequeteManager.class.php';
$requeteManage=new RequeteManager();
if(isset($_POST["action"])&&isset($_POST["id"])&&$_POST["action"]=="delete"){
	$rep=$requeteManage->removeRequete($_POST["id"]);
	if(gettype($rep)===gettype("")){
		$messageType="error";
		$messageText=$rep;
	}else{
		$messageType="info";
		$messageText="1 row removed";
	}
}
$list=$requeteManage->getRequetesArray();
include_once __DIR__.'/template/tableaux.php';
?>