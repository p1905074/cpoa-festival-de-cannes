<?php

$table="VIP";
$messageType="null";
include_once '../model/VIPManager.class.php';
$vipManage=new VIPManager();
if(isset($_POST["action"])&&isset($_POST["id"])&&$_POST["action"]=="delete"){
	$rep=$vipManage->removeVIP($_POST["id"]);
	if(gettype($rep)===gettype("")){
		$messageType="error";
		$messageText=$rep;
	}else{
		$messageType="info";
		$messageText="1 row removed";
	}
}
$list=$vipManage->getVIPArray();
include_once __DIR__.'/template/tableaux.php';
?>