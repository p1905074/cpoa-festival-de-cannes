<?php
include_once '../model/VIPManager.class.php';
$vipManager=new VIPManager();
$messageType="null";
if(isset($_GET["id"])){
	$id=$_GET["id"];
}
if(isset($_POST["action"])&&isset($id)){
	switch ($_POST["action"]) {
		case 'deleteAssociation':
			if(isset($_POST["id"])&&isset($_POST["role"])){
				$rep=$vipManager->removeFilmAssociation($_POST["id"],$_POST["role"]);
			}
			break;
		case 'addAssociation':
			if(isset($_POST["title"])&&isset($_POST["role"])){
				$rep=$vipManager->addFilmToVIP($id,$_POST["title"],$_POST["role"]);
			}
			break;
		case 'deleteInvitation':
			if(isset($_POST["id"])){
				$rep=$vipManager->removeInvitation($_POST["id"]);
			}
			break;
		case 'addInvitation':
			if(isset($_POST["role"])&&isset($_POST["date"])){
				$rep=$vipManager->addInvitationForVIP($id,$_POST["role"],$_POST["date"]);
			}
			break;
		default:
			$rep="action inconnu";
	}
	if(gettype($rep)===gettype("")){
		$messageType="error";
		$messageText=$rep;
	}else{
		$messageType="info";
		$messageText="1 ligne changé";
	}
}elseif(isset($_POST["firstName"])&&isset($_POST["lastName"])&&isset($_POST["nationality"])&&isset($_POST["sexe"])){
	if(isset($id)){
		$rep=$vipManager->editVIP($id,$_POST["firstName"],$_POST["lastName"],$_POST["nationality"],$_POST["sexe"],isset($_POST["photo"])?$_POST["photo"]:"null",isset($_POST["tel"])?$_POST["tel"]:"null",isset($_POST["mail"])?$_POST["mail"]:"null",isset($_POST["description"])?$_POST["description"]:"null");
		if(gettype($rep)===gettype("")){
			$messageType="error";
			$messageText=$rep;
		}else{
			$messageType="info";
			$messageText="1 ligne changé";
		}
	}else{
		$rep=$vipManager->addVIP("null",$_POST["firstName"],$_POST["lastName"],$_POST["nationality"],$_POST["sexe"],isset($_POST["photo"])?$_POST["photo"]:"null",isset($_POST["tel"])?$_POST["tel"]:"null",isset($_POST["mail"])?$_POST["mail"]:"null",isset($_POST["description"])?$_POST["description"]:"null");
		if(gettype($rep)===gettype("")){
			$messageType="error";
			$messageText=$rep;
		}else{
			$messageType="info";
			$messageText="1 ligne ajouté";
		}
	}

}
?>
<?php
if(isset($id)){
        $obj=$vipManager->getVIP($_GET["id"]);
        $firstName=$obj->getFirstName();
        $lastName=$obj->getLastName();
        $nationality=$obj->getNationality();
        $sexe=$obj->getSexe();
        $photo=$obj->getPhoto();
        $tel=$obj->getTel();
        $mail=$obj->getMail();
        $description=$obj->getDescription();
        
}else{
        $firstName="";
        $lastName="";
        $nationality="";
        $sexe="";
        $photo="";
        $tel="";
        $mail="";
        $description="";
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php 
        if(isset($id)){
                echo "Profil de ".$firstName." ".$lastName;
        }else{
                echo "ajout VIP";
        }
        ?></title>
	<link rel="stylesheet" type="text/css" href="style/main.css">
</head>
<body>
	<?php
	if ($messageType!="null") {
	?>
	<div class="bandeau <?php echo $messageType ?>"> <?php echo $messageText?> </div>
	<?php
	}
	?>
	<a href=".." class="home"></a>
        <h1><?php
        if(isset($id)){
                echo "Edition du profil de ".$firstName." ".$lastName;
        }else{               
                echo "Ajout d'un VIP";
        }
        ?></h1>
	<a href="listVIP.php"><- go back</a>
<form method="post">
	<label>prenom :<input type="text" name="firstName" value="<?php echo $firstName?>"></label><br>
	<label>nom :<input type="text" name="lastName" value="<?php echo $lastName?>"></label><br>
	<label>sexe :
	<select name="sexe" value="<?php echo $sexe?>">
        <?php 
        $sexeList=listColumn("sexe","label");
        for ($i=0; $i < sizeof($sexeList); $i++) { 
        ?>
        <option value="<?php echo $sexeList[$i]?>" <?php if($sexeList[$i]==$sexe){echo "selected=''";}?>><?php echo $sexeList[$i] ?></option>
        <?php
        }
        ?>
	</select>
	</label><br>
	<label>nationalité :
	<select name="nationality" value="<?php echo $nationality?>">
        <?php 
        $nationalitiesAbrev=listColumn("nationality","label");
        $nationalitiesFull=listColumn("nationality","full_label");
        for ($i=0; $i < sizeof($nationalitiesAbrev); $i++) { 
        ?>
        <option value="<?php echo $nationalitiesAbrev[$i]?>" <?php if($nationalitiesAbrev[$i]==$nationality){echo "selected=''";}?>><?php echo $nationalitiesFull[$i] ?></option>
        <?php
        }
        ?>
	</select>
	</label><br>
	<label>photo :<input type="text" name="photo" value="<?php echo $photo?>"></label><br>
	<img src="img/vip/<?php echo $photo ?>">
	Description:<br>
	<textarea name=description><?php echo $description?></textarea>
        <input type="submit" name="" value="enregistrer"><br>
</form>
<?php
	if(isset($id)){
?>
<table>
	<tr>
		<th>Liste des films</th>
		<th>Liste des invitations</th>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
				<td>Titre</td>
				<td>Rôle</td>
				<td></td>
				</tr>
				<?php
				$films=$vipManager->filmFromVIP($id);
				for ($i=0; $i < sizeof($films); $i++) { 
					?>
				<tr>
					<td style="width: 30%"><?php echo $films[$i][0]?></td>
					<td style="width: 40%"><?php echo $films[$i][1];?></td>
					<td  style="width: 30%">
						<form method="post">
						<button class="delete" onclick="this.parentElement.submit()">supprimer</button>
						<input type="hidden" name="action" value="deleteAssociation">
						<input type="hidden" name="id" value="<?php echo $films[$i][2] ?>">
						<input type="hidden" name="role" value="<?php echo $films[$i][1] ?>">
						</form>
					</td>
				</tr>
					<?php
				}
				?>
				<tr>
					<td colspan="3">
						<form method="post">
				<select style="width:30%" name="title">
                                        <?php
                                        $titles=listColumn("Film","title");
                                        for ($i=0; $i < sizeof($titles); $i++) { 
                                        ?>
                                        <option value="<?php echo $titles[$i]?>"><?php echo $titles[$i]; ?></option>
                                        <?php
                                        }
                                        ?>                            
                                </select>
                                <select style="width:30%" name="role">
                                        <option value="metteurEnScene">metteur en scene</option>
                                        <option value="realisateur">réalisateur</option>
                                        <option value="acteur">acteur</option>
                                </select>
				<input type="hidden" name="action" value="addAssociation">
				<button style="width:30%;float: right;" class="add" onclick="this.parentElement.submit()">ajouter</button>
				</form>
				</td>
				</tr>
			</table>
		</td>
		<td >
			<table>
				<tr>
				<td>Rôle</td>
				<td>Date</td>
				<td></td>
				</tr>
				<?php
				$invits=$vipManager->invitationOfVIP($id);
				for ($i=0; $i < sizeof($invits); $i++) { 
					?>
				<tr>
					<td style="width: 30%"><?php echo $invits[$i][0]?></td>
					<td style="width: 40%"><?php echo $invits[$i][1];?></td>
					<td  style="width: 30%">
						<form method="post">
						<button class="delete" onclick="this.parentElement.submit()">supprimer</button>
						<input type="hidden" name="action" value="deleteInvitation">
						<input type="hidden" name="id" value="<?php echo $invits[$i][2] ?>">
						<input type="hidden" name="role" value="<?php echo $invits[$i][1] ?>">
						</form>
					</td>
				</tr>
					<?php
				}
				?>
				<tr>
					<td colspan="3">
						<form method="post">
				<select style="width:30%" name="role">
                                        <?php
                                        $roles=listColumn("role","label");
                                        for ($i=0; $i < sizeof($roles); $i++) { 
                                        ?>
                                        <option value="<?php echo $roles[$i]?>"><?php echo $roles[$i]; ?></option>
                                        <?php
                                        }
                                        ?>                            
                                </select>
				<input style="width:30%" type="date" name="date">
				<input type="hidden" name="action" value="addInvitation">
				<button style="width:30%;float: right;" class="add" onclick="this.parentElement.submit()">ajouter</button>
				</form>
				</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php
	}
?>
</body>