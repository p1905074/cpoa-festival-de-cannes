<?php
/**
 * class acteur
 */

include_once __DIR__.'/connectBdd.php';
include_once __DIR__.'/../controller/User.class.php';
class UserManager
{
	public function verifyUser($mail,$password){
		if(strlen($mail)>256){
			return false;
		}
		$sql="SELECT * FROM user WHERE mail='".$mail."' and hash='".md5($password)."' limit 1";
		$row=$GLOBALS["bddManage"]->query("SELECT * FROM user WHERE mail=? and hash=? limit 1",array($mail,md5($password)))->fetch();
		if($row){
			return $row["id"];
		}
		return false;
	}
	public function connectUser($id){
		$_SESSION["connected"]=true;
		$_SESSION["admin"]=true;
		$obj=$this->getUser($id);
		$_SESSION["admin"]=$obj->getAdmin();
		$_SESSION["user"]=$obj;
	}
	public function getUsers()
	{
		if($_SESSION["admin"]!=true)return "admin permission not granted";
		$users=array();
		$result=$GLOBALS["bddManage"]->query("SELECT * FROM user",array());
		while($row=$result->fetch()){
			if(!$row)return null;
			$obj=new User();
			$obj->setId($row["id"]);
			$obj->setFirstName($row["firstName"]);
			$obj->setLastName($row["lastName"]);
			$obj->setMail($row["mail"]);
			$obj->setTel($row["tel"]);
			$obj->setHash($row["hash"]);
			$obj->setAdmin($row["is_admin"]);
			$users[]=$obj;
		}
		return $users;
	}
	public function addUser($id,$firstName,$lastName,$mail,$tel,$password,$admin)
	{
		if($_SESSION["admin"]!=true)return "admin permission not granted";
		$obj=new User();
		if(!$obj->setId($row["id"]))return "invalid field id";
		if(!$obj->setFirstName($row["firstName"]))return "invalid field firstName";
		if(!$obj->setLastName($row["lastName"]))return "invalid field lastName";
		if(!$obj->setMail($row["mail"]))return "invalid field mail";
		if(!$obj->setTel($row["tel"]))return "invalid field tel";
		if(!$obj->setAdmin($row["is_admin"]))return "invalid field admin";
		if(!$obj->setPassword($password))return "invalid password (must have more than 8 character, have 1 special char, 1 number, 1 lower case and 1 upper case)";
		$obj->insertRow();
		$GLOBALS["bddManage"]->query("INSERT INTO user (writer,reader,admin,login,hash,id ) VALUES ( ?, ?, ?, ?,?,? )",array($write,$read,$admin,$login,$hash,$id=="null"?null:$id));
		return true;
	}
	public function removeUser($id)
	{
		if($_SESSION["admin"]!=true)return "admin permission not granted";
		$GLOBALS["bddManage"]->query("DELETE FROM user WHERE id=?",array($id));
		return true;
	}
	public function getUser($id)
	{
		if($_SESSION["admin"]!=true)return "admin permission not granted";
		$row=$GLOBALS["bddManage"]->query("SELECT * FROM user WHERE id=? limit 1",array($id))->fetch();
		if(!$row)return null;
		$obj=new User();
		$obj->setId($row["id"]);
		$obj->setFirstName($row["firstName"]);
		$obj->setLastName($row["lastName"]);
		$obj->setMail($row["mail"]);
		$obj->setTel($row["tel"]);
		$obj->setHash($row["hash"]);
		$obj->setAdmin($row["is_admin"]);
		return $obj;
	}
	public function editUser($id,$firstName,$lastName,$mail,$tel,$password,$admin)
	{
		if($rep=addUser(-1,$firstName,$lastName,$mail,$tel,$password,$admin)){
			removeUser($id);
			$GLOBALS["bddManage"]->query("UPDATE user SET id=? WHERE id=-1",array($id));
		}else{
			return $rep;
		}

		return true;

	}
}
?>