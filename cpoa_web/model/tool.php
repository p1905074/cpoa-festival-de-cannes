<?php
function isValideDate($date, $format = 'd-m-Y')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
function isDefine($table,$column,$value)
{
	if($GLOBALS["bddManage"]->query("SELECT * FROM ".$table." WHERE ".$column."=?",array($value))->fetch())return true;
	return false;
}
function startWith( $haystack, $needle ) {
     $length = strlen( $needle );
     return substr( $haystack, 0, $length ) === $needle;
}

function endWith( $haystack, $needle ) {
    $length = strlen( $needle );
    if( !$length ) {
        return true;
    }
    return substr( $haystack, -$length ) === $needle;
}
function listColumn($table,$column)
{
    $res=$GLOBALS["bddManage"]->query("SELECT ".$column." FROM ".$table,array());
    $list=array();
    while ($row=$res->fetch()) {
        $list[]=$row[$column];
    }
    return $list;
}
?>