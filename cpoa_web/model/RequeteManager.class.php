<?php
/**
 * class requete
	
 */
include_once __DIR__.'/connectBdd.php';
include_once __DIR__.'/../controller/Requete.class.php';
class RequeteManager
{
	public function getRequete($id)
	{
		$result=$GLOBALS["bddManage"]->query("SELECT * FROM requete WHERE id=?",array($id));
		$row=$result->fetch();
		$obj=new Requete();
		$obj->setTitle($row["title"]);
		$obj->setDescription($row["description"]);
		$obj->setQueryDate($row["queryDate"]);
		$obj->setForThe($row["forThe"]);
		$obj->setPersonneId($row["personne_id"]);
		$obj->setId($row["id"]);
		return $obj;
	}
	public function getRequetesArray()
	{
		$requetes=array();
		$result=$GLOBALS["bddManage"]->query("SELECT title,id FROM requete",array());
		while($row=$result->fetch()){
			$line=array();
			$line[]=$row["title"];
			$line[]=$row["id"];
			$requetes[]=$line;
		}
		return $requetes;
	}
	public function addRequete($id,$title,$description,$queryDate,$forThe,$personne_id)
	{
		$obj=new Requete();
		if(!$obj->setTitle($title))return "titre invalide";
		if(!$obj->setDescription($description))return "description invalide";
		if(!$obj->setQueryDate($queryDate))return "date de demande invalide";
		if(!$obj->setForThe($forThe))return "date butoire invalide";
		if(!$obj->setPersonneId($personne_id))return "comanditaire invalide";
		if(!$obj->setId($id))return "id invalide";
		$GLOBALS["bddManage"]->query("INSERT INTO requete (title, description, queryDate, forThe,personne_id, id) VALUES ( ?, ?, ?, ?, ?, ? )",array($title,$description,$queryDate,$forThe,$personne_id,$id=="null"?null:$id));
		return true;
	}
	public function removeRequete($id)
	{
		$GLOBALS["bddManage"]->query("DELETE FROM requete WHERE id=?",array($id));
		return true;
	}
	public function editRequete($oldId,$title,$description,$queryDate,$forThe,$personne_id)
	{
		if($rep=$this->addRequete(-1,$title,$description,$queryDate,$forThe,$personne_id)){
			$this->removeRequete($oldId);
			$GLOBALS["bddManage"]->query("UPDATE requete SET id=? WHERE id=-1",array($oldId));
		}else{
			return $rep;
		}

		return true;
	}
}
?>