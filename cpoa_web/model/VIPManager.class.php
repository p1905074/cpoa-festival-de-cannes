<?php
/**
 * class vip
 */
include_once __DIR__.'/connectBdd.php';
include_once __DIR__.'/../controller/VIP.class.php';
class VIPManager
{
	public function getVIP($id)
	{
		$result=$GLOBALS["bddManage"]->query("SELECT p.*, s.label as sexe, n.label as nationality FROM personne p LEFT OUTER JOIN nationality n on n.id=p.nationality_id LEFT OUTER JOIN sexe s on s.id=p.sexe_id WHERE p.id=?",array($id));
		$row=$result->fetch();
		$obj=new VIP();
		$obj->setFirstName($row["firstName"]);
		$obj->setLastName($row["lastName"]);
		$obj->setNationality($row["nationality"]);
		$obj->setSexe($row["sexe"]);
		$obj->setPhoto($row["photo"]);
		$obj->setTel($row["tel"]);
		$obj->setMail($row["mail"]);
		$obj->setDescription($row["description"]);
		$obj->setId($row["id"]);
		return $obj;
	}
	public function getVIPArray()
	{
		$vips=array();
		$result=$GLOBALS["bddManage"]->query("SELECT lastName, firstName,id FROM personne",array());
		while($row=$result->fetch()){
			$line=array();
			$line[]=$row["firstName"]." ".$row["lastName"];
			$line[]=$row["id"];
			$vips[]=$line;
		}
		return $vips;
	}
	public function addVIP($id,$firstName,$lastName,$nationality,$sexe,$photo,$tel,$mail,$description)
	{
		$obj=new VIP();
		if(!$obj->setFirstName($firstName))return "prenom invalide";
		if(!$obj->setLastName($lastName))return "nom de famille invalide";
		if(!$obj->setNationality($nationality))return "nationalité invalide";
		if(!$obj->setSexe($sexe))return "date butoire invalide";
		if(!$obj->setPhoto($photo))return "photo invalide";
		if(!$obj->setTel($tel))return "numero de telephone invalide";
		if(!$obj->setMail($mail))return "email invalide";
		if(!$obj->setDescription($description))return "description invalide";
		if(!$obj->setId($id))return "id invalide";
		$nationality_id=$GLOBALS["bddManage"]->query("SELECT id FROM nationality WHERE label=?",array($nationality))->fetch()["id"];
		$sexe_id=$GLOBALS["bddManage"]->query("SELECT id FROM sexe WHERE label=?",array($sexe))->fetch()["id"];
		$GLOBALS["bddManage"]->query("INSERT INTO personne (firstName, lastName, nationality_id, sexe_id,photo,tel,mail,description, id) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? , ? )",
			array($firstName,
				$lastName,
				$nationality_id,
				$sexe_id,
				($photo=="null"?null:$photo),
				($tel=="null"?null:$tel),
				($mail=="null"?null:$mail),
				($description=="null"?null:$description),
				($id=="null"?null:$id)));
		return true;
	}
	public function filmFromVIP($VIP_id){
		$films=array();
		$result=$GLOBALS["bddManage"]->query("SELECT f.title as title,r.id as id FROM Film f LEFT OUTER JOIN realisateur r on r.film_id=f.id WHERE r.personne_id=?",array($VIP_id));
		while($row=$result->fetch()){
			$line=array();
			$line[]=$row["title"];
			$line[]="realisateur";
			$line[]=$row["id"];
			$films[]=$line;
		}
		$result=$GLOBALS["bddManage"]->query("SELECT f.title as title,m.id as id FROM Film f LEFT OUTER JOIN metteurEnScene m on m.film_id=f.id WHERE m.personne_id=?",array($VIP_id));
		while($row=$result->fetch()){
			$line=array();
			$line[]=$row["title"];
			$line[]="metteurEnScene";
			$line[]=$row["id"];
			$films[]=$line;
		}
		$result=$GLOBALS["bddManage"]->query("SELECT f.title as title,a.id as id FROM Film f LEFT OUTER JOIN acteur a on a.film_id=f.id WHERE a.personne_id=?",array($VIP_id));
		while($row=$result->fetch()){
			$line=array();
			$line[]=$row["title"];
			$line[]="acteur";
			$line[]=$row["id"];
			$films[]=$line;
		}
		return $films;
	}
	public function addFilmToVIP($VIP_id,$title,$role){
		if(!isDefine("Film","title",$title))return "Aucun film correspondant";
		$film_id=$GLOBALS["bddManage"]->query("SELECT id FROM Film WHERE title=?",array($title))->fetch()["id"];
		switch ($role) {
			case 'realisateur':
				$GLOBALS["bddManage"]->query("INSERT INTO realisateur (personne_id,film_id,id) VALUES (?,?,null)",array($VIP_id,$film_id));
				break;
			case 'metteurEnScene':
				$GLOBALS["bddManage"]->query("INSERT INTO metteurEnScene (personne_id,film_id,id) VALUES (?,?,null)",array($VIP_id,$film_id));
				break;
			case 'acteur':
				$GLOBALS["bddManage"]->query("INSERT INTO acteur (personne_id,film_id,id) VALUES (?,?,null)",array($VIP_id,$film_id));
				break;
			default:
				return "role invalide";
		}
		return true;
	}
	public function removeFilmAssociation($association_id,$role){
		switch ($role) {
			case 'realisateur':
				$GLOBALS["bddManage"]->query("DELETE FROM realisateur WHERE id=?",array($association_id));
				break;
			case 'metteurEnScene':
				$GLOBALS["bddManage"]->query("DELETE FROM metteurEnScene WHERE id=?",array($association_id));
				break;
			case 'acteur':
				$GLOBALS["bddManage"]->query("DELETE FROM acteur WHERE id=?",array($association_id));
				break;
			default:
				return "role invalide";
		}
	}
	public function addInvitationForVIP($VIP_id,$role,$date){
		if(!isDefine("role","label",$role))return "Aucun role ne porte le nom de '".$role."'";
		$role_id=$GLOBALS["bddManage"]->query("SELECT id FROM role WHERE label=?",array($role))->fetch()["id"];
		$GLOBALS["bddManage"]->query("INSERT INTO invitation (role_id,invite_date,personne_id,id) VALUES (?,?,?,null)",array($role_id,$date,$VIP_id));
	}
	public function removeInvitation($invitation_id){
		$GLOBALS["bddManage"]->query("DELETE FROM invitation WHERE id=?",array($invitation_id));
	}


	public function invitationOfVIP($VIP_id){
		$result=$GLOBALS["bddManage"]->query("SELECT i.invite_date as invite_date, r.label as role, i.id as id FROM invitation i LEFT OUTER JOIN role r on i.role_id=r.id WHERE i.personne_id=?",array($VIP_id));
		$invite=array();
		while($row=$result->fetch()){
			$line=array();
			$line[]=$row["role"];
			$line[]=$row["invite_date"];
			$line[]=$row["id"];
			$invite[]=$line;
		}
		return $invite;
	}
	public function removeVIP($id)
	{
		$GLOBALS["bddManage"]->query("SET foreign_key_checks=0",array());
		$GLOBALS["bddManage"]->query("DELETE FROM personne WHERE id=?",array($id));
		$GLOBALS["bddManage"]->query("SET foreign_key_checks=1",array());
		return true;
	}
	public function editVIP($oldId,$firstName,$lastName,$nationality,$sexe,$photo,$tel,$mail,$description)
	{
		if($rep=$this->addVIP(-1,$firstName,$lastName,$nationality,$sexe,$photo,$tel,$mail,$description)){
			$this->removeVIP($oldId);
			$GLOBALS["bddManage"]->query("UPDATE personne SET id=? WHERE id=-1",array($oldId));
		}else{
			return $rep;
		}
		return true;
	}
}
?>