﻿<?php
/**
 * class de conection a la base de donné
 */
class BddConnectionManager
{
	private	$host;
	private $base;
	private $user;
	private $password;
	private $baseDeDonne;

	static function connect($host,$base,$user,$password){
		try{
			$bdd=new PDO("mysql:host=".$host.";dbname=".$base.";charset=utf8",$user,$password);
			return $bdd;
		}catch(exception $e){
			return null;
		}
	}
	function __construct($host,$base,$user,$password)
	{
		$this->host=$host;
		$this->base=$base;
		$this->user=$user;
		$this->password=$password;
		$this->baseDeDonne=BddConnectionManager::connect($host,$base,$user,$password);
	}
	public function query($sql,$arguments)
	{
		try{
			$qu=$this->baseDeDonne->prepare($sql);
			$qu->execute($arguments);
		}catch(Exception $e){
			echo $e->getMessage();
		}
		return $qu;
	}
}
?>