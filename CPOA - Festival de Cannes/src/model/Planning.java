package model;

public class Planning {
    private int idPlanning;
    private String nomPlanning;
    private int idCat;

    public Planning(int idPlanning, String nomPlanning, int idCat) {
        this.idPlanning = idPlanning;
        this.nomPlanning = nomPlanning;
        this.idCat = idCat;
    }

    public int getIdCat() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }

    public int getIdPlanning() {
        return idPlanning;
    }

    public void setIdPlanning(int idPlanning) {
        this.idPlanning = idPlanning;
    }

    public String getNomPlanning() {
        return nomPlanning;
    }

    public void setNomPlanning(String nomPlanning) {
        this.nomPlanning = nomPlanning;
    }

    @Override
    public String toString() {
        return "Planning{" +
                "idPlanning=" + idPlanning +
                ", nomPlanning='" + nomPlanning + '\'' +
                ", idCat=" + idCat +
                '}';
    }
}
