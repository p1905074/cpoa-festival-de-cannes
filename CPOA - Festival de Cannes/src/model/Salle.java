package model;

import java.util.Collection;

public class Salle {
   private int numeroSalle;
   private String nomSalle;
   private int nombrePlace;
   private int idCategorie;
   private int numHoraire;

   public Salle(int numeroSalle, String nomSalle, int nombrePlace, int idCategorie, int numHoraire) {
      this.numeroSalle = numeroSalle;
      this.nomSalle = nomSalle;
      this.nombrePlace = nombrePlace;
      this.idCategorie = idCategorie;
      this.numHoraire = numHoraire;
   }

   public int getNumeroSalle() {
      return numeroSalle;
   }

   public void setNumeroSalle(int numeroSalle) {
      this.numeroSalle = numeroSalle;
   }

   public String getNomSalle() {
      return nomSalle;
   }

   public void setNomSalle(String nomSalle) {
      this.nomSalle = nomSalle;
   }

   public int getNombrePlace() {
      return nombrePlace;
   }

   public void setNombrePlace(int nombrePlace) {
      this.nombrePlace = nombrePlace;
   }

   public int getidCategorie() {
      return idCategorie;
   }

   public void setidCategorie(int idCategorie) {
      this.idCategorie = idCategorie;
   }

   public int getNumHoraire() {
      return numHoraire;
   }

   public void setNumHoraire(int numHoraire) {
      this.numHoraire = numHoraire;
   }

   @Override
   public String toString() {
      return "Salle{" + "numero de la Salle:" + numeroSalle +", nom de la Salle:'" + nomSalle +", nombrePlace:" + nombrePlace +", id de la Categorie:" + idCategorie +", num de l'horaire:" + numHoraire +'}';
   }
}