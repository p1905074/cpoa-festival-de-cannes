package model;


import java.sql.Time;
import java.util.Collection;

public class Film {

   private String nomFilm;
   private Time duree;
   private int idCategorie;
   private int numHoraire;
   private int numFilm;






   public Film(String nomFilm, int numFilm, Time duree, int numHoraire, int idCategorie) {
      this.nomFilm = nomFilm;
      this.numFilm = numFilm;
      this.duree = duree;;
      this.numHoraire = numHoraire;
      this.idCategorie = idCategorie;
   }

   public String getNomFilm() {
      return nomFilm;
   }
   public void setNomFilm(String newNomFilm) {
      nomFilm = newNomFilm;
   }
   public int getNumFilm() {
      return numFilm;
   }
   public void setNumFilm(int newNumFilm) {
      numFilm = newNumFilm;
   }
   public Time getDuree() {
      return duree;
   }
   public void setDuree(Time newDuree) {
      duree = newDuree;
   }

   public int getidCategorie() {
      return idCategorie;
   }

   public void setidCategorie(int idCategorie) {
      this.idCategorie = idCategorie;
   }

   public int getNumHoraire() {
      return numHoraire;
   }

   public void setNumHoraire(int numHoraire) {
      this.numHoraire = numHoraire;
   }

   @Override
   public String toString() {
      return "Film{" +
              "nom du Film:" + nomFilm +", numero du film:" + numFilm +", duree:" + duree +", horaire du film:" + numHoraire +", categorie: " + idCategorie +'}';
   }
}