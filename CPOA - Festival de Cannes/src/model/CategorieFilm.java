package model;


public class CategorieFilm {

   private String nomCategorie;
   private int idCategorie;
   private int idJury;

    public CategorieFilm(String nomCategorie, int idCategorie, int idJury) {
        this.nomCategorie = nomCategorie;
        this.idCategorie = idCategorie;
        this.idJury = idJury;
    }

    public int getIdJury() {
        return idJury;
    }

    public void setIdJury(int idJury) {
        this.idJury = idJury;
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    public int getidCategorie() {
        return idCategorie;
    }

    public void setidCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }
    
    @Override
    public String toString() {
        return "CategorieFilm{" + "nom:" + nomCategorie + ", idCategorie:" + idCategorie + ", idJury:" + idJury +'}';
    }
}