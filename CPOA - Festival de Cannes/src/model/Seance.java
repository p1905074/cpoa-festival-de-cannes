package model;

import java.sql.Time;

public class Seance {
    private final int idSeance;
    private final int idSalle;
    private final int idFilm;
    private final Time debutFilm;
    private final int idPlanning;
    private final int day;

    public Seance(int idSeance, int idSalle, int idFilm, Time debutFilm, int idPlanning,int day) {
        this.idSeance = idSeance;
        this.idSalle = idSalle;
        this.idFilm = idFilm;
        this.debutFilm = debutFilm;
        this.idPlanning = idPlanning;
        this.day = day;
    }

    public int getDay() {
        return day;
    }

    public int getIdSeance() {
        return idSeance;
    }

    public int getIdSalle() {
        return idSalle;
    }

    public int getIdFilm() {
        return idFilm;
    }

    public Time getDebutFilm() {
        return debutFilm;
    }

    public int getIdPlanning() {
        return idPlanning;
    }

    @Override
    public String toString() {
        return "Seance{" +
                "idSeance=" + idSeance +
                ", idSalle=" + idSalle +
                ", idFilm=" + idFilm +
                ", debutFilm=" + debutFilm +
                ", idPlanning=" + idPlanning +
                ", idPlanning=" + day +
                '}';
    }
}

