package model;

public class Jury {
   private String nomJury;
   private String prenomJury;
   private String categorie;
   private int idJury;


   public Jury(String categorie, int idJury, String nomJury, String prenomJury) {
      this.categorie = categorie;
      this.idJury = idJury;
      this.nomJury = nomJury;
      this.prenomJury = prenomJury;
   }

   public String getCategorie() {
      return categorie;
   }

   public void setCategorie(String categorie) {
      this.categorie = categorie;
   }

   public int getIdJury() {
      return idJury;
   }

   public void setIdJury(int idJury) {
      this.idJury = idJury;
   }

   public String getNomJury() {
      return nomJury;
   }

   public void setNomJury(String nomJury) {
      this.nomJury = nomJury;
   }

   public String getPrenomJury() {
      return prenomJury;
   }

   public void setPrenomJury(String prenomJury) {
      this.prenomJury = prenomJury;
   }

   @Override
   public String toString() {
      return "Jury{" + " categorie:" + categorie + ", idJury:" + idJury + ", nomJury:" + nomJury + ", prenomJury:" + prenomJury +  + '}';
   }
}