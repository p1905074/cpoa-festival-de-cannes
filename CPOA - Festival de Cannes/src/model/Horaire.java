package model;

public class Horaire {
  private int numHoraire;
  private int numFilm;
  public int numSalle;
  private int heureHoraire;
  private int jourHoraire;



   public Horaire(int numHoraire, int jourHoraire, int heureHoraire, int numFilm, int numSalle) {
      this.numHoraire = numHoraire;
      this.jourHoraire = jourHoraire;
      this.heureHoraire = heureHoraire;
      this.numFilm = numFilm;
      this.numSalle = numSalle;
   }

    public int getNumHoraire() {
        return numHoraire;
    }

    public void setNumHoraire(int numHoraire) {
        this.numHoraire = numHoraire;
    }

    public int getJourHoraire() {
        return jourHoraire;
    }

    public void setJourHoraire(int jourHoraire) {
        this.jourHoraire = jourHoraire;
    }

    public int getHeureHoraire() {
        return heureHoraire;
    }

    public void setHeureHoraire(int heureHoraire) {
        this.heureHoraire = heureHoraire;
    }

    public int getNumFilm() {
        return numFilm;
    }

    public void setNumFilm(int numFilm) {
        this.numFilm = numFilm;
    }

    public int getNumSalle() {
        return numSalle;
    }

    public void setNumSalle(int numSalle) {
        this.numSalle = numSalle;
    }

    @Override
    public String toString() {
        return "Horaire{" +"numero de l'horaire:" + numHoraire +", jour de l'horaire:" + jourHoraire +", heure de l'horaire: " + heureHoraire +", film: " + numFilm +", salle=" + numSalle +'}';
    }
}